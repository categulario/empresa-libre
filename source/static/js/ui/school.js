

var toolbar_students = [
    {view: 'button', id: 'cmd_new_student', label: 'Nuevo', type: 'iconButton',
        autowidth: true, icon: 'user-plus'},
    {view: 'button', id: 'cmd_edit_student', label: 'Editar', type: 'iconButton',
        autowidth: true, icon: 'user'},
    {view: 'button', id: 'cmd_delete_student', label: 'Eliminar', type: 'iconButton',
        autowidth: true, icon: 'user-times'},
]


var grid_cols_students = [
    {id: 'index', header:'#', css: 'right',
        footer: {content: 'countRows', colspan: 2, css: 'right'}},
    {id: 'id', header: 'Clave', sort: 'int', css: 'right'},
    {id: 'nombre', header: ['Nombre', {content: 'textFilter'}],
        sort: 'string'},
    {id: 'paterno', header: ['A. Paterno', {content: 'textFilter'}],
        sort: 'string'},
    {id: 'materno', header: ['A. Materno', {content: 'textFilter'}],
        sort: 'string'},
    {id: 'rfc', header: ['RFC', {content: 'textFilter'}], adjust: 'data',
        sort: 'string'},
    {id: 'curp', header: ['CURP'], sort: 'string'},
]


var grid_students = {
    view: 'datatable',
    id: 'grid_students',
    select: 'row',
    adjust: true,
    footer: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_cols_students,
    ready:function(){
        this.adjustColumn('index');
        this.adjustColumn('id');
        this.adjustColumn('nombre');
        this.adjustColumn('rfc');
        this.adjustColumn('curp');
    },
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i+1;
            })
        }
    },
}


var rows_school_home = [
    {view: 'toolbar', elements: toolbar_students},
    grid_students,
]


var student_controls_generales = [
    {view: 'text', id: 'student_name', name: 'nombre', label: 'Nombre: ',
        required: true, invalidMessage: 'El nombre es requerido'},
    {view: 'text', id: 'student_paterno', name: 'paterno', label: 'Apellido Paterno: ',
        required: true, invalidMessage: 'El apellido paterno es requerido'},
    {view: 'text', id: 'student_materno', name: 'materno',
        label: 'Apellido Materno: '},
    {cols: [
        {view: 'text', id: 'student_rfc', name: 'rfc', label: 'RFC: ',
            invalidMessage: 'RFC inválido', adjust: 'data',
            attributes: {maxlength: 13}},
        {view: 'text', id: 'student_curp', name: 'curp', label: 'CURP: ',
            required: true, invalidMessage: 'CURP inválido', adjust: 'data',
            attributes: {maxlength: 20}},
    {}]},
    {cols: [
        {view: 'richselect', id: 'student_grupo', name: 'grupo',
            label: 'Nivel Educativo: ', required: true, options: [],
            invalidMessage: 'El Nivel Educativo es requerido'},
        {},
    ]},
]


var form_controls_student = [
    {
        view: 'tabview',
        id: 'tab_student',
        tabbar: {options: ['Datos Generales']}, animate: true,
        cells: [
            {id: 'Datos Generales', rows: student_controls_generales},
        ]
    },
    {rows: [
        { template:"", type: "section" },
        { margin: 10, cols: [{},
            {view: "button", id: "cmd_save_student", label: "Guardar" ,
                type: "form", autowidth: true, align: "center"},
            {view: "button", id: "cmd_cancel_student", label: "Cancelar" ,
                type: "danger", autowidth: true, align: "center"},
            {}]
        },
    ]}
]


var form_student = {
    type: 'space',
    cols: [{
        view: 'form',
        id: 'form_student',
        complexData: true,
        scroll: true,
        elements: form_controls_student,
        elementsConfig: {
            labelWidth: 150,
            labelAlign: 'right'
        },
        rules: {
            nombre: function(value){ return value.trim() != '';},
            curp: validate_curp,
        }
    }]
}


var multi_school = {
    id: 'multi_school',
    view: 'multiview',
    animate: true,
    cells:[
        {id: 'school_home', rows: rows_school_home},
        {id: 'school_groups', rows: []},
        {id: 'new_student', rows: [form_student]},
    ],
}


var app_school = {
    id: 'app_school',
    rows:[
        {view: 'template', id: 'th_school', type: 'header',
            template: 'Administración de Escuela'},
        multi_school
    ],
}
