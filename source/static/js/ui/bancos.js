

var toolbar_banco = [
    {view: 'richselect', id: 'lst_cuentas_banco', label: 'Cuenta',
        labelWidth: 100, options: []},
    {view: 'text', id: 'txt_cuenta_moneda', label: 'Moneda', readonly: true},
    {view: 'currency', id: 'txt_cuenta_saldo', label: 'Saldo', readonly: true,
        inputAlign: 'right', value: 0}
]


var toolbar_filtro_cuenta = [
    {view: 'richselect', id: 'filter_cuenta_year', label: 'Año',
        labelAlign: 'right', labelWidth: 50, width: 150, options: []},
    {view: 'richselect', id: 'filter_cuenta_month', label: 'Mes',
        labelAlign: 'right', labelWidth: 50, width: 200, options: months},
    {view: 'daterangepicker', id: 'filter_cuenta_dates', label: 'Fechas',
        labelAlign: 'right', width: 300},
    {},
]


var toolbar_movimientos_banco = [
    {view: 'button', id: 'cmd_agregar_retiro', label: 'Retiro',
        type: 'iconButton', autowidth: true, icon: 'minus'},
    {view: 'button', id: 'cmd_agregar_deposito', label: 'Depósito',
        type: 'iconButton', autowidth: true, icon: 'plus'},
    {},
    {view: 'button', id: 'cmd_cancelar_movimiento', label: 'Cancelar',
        type: 'iconButton', autowidth: true, icon: 'ban'},
]


var grid_cuentabanco_cols = [
    {id: 'id', header:'ID', hidden: true},
    {id: 'fecha', header: 'Fecha', width: 150},
    {id: 'numero_operacion', header: 'Referencia'},
    {id: 'descripcion', header: ['Descripción', {content: 'textFilter'}],
        fillspace:true},
    {id: 'retiro', header: ['Retiro', {content: 'numberFilter'}],
        width: 125, format: webix.i18n.priceFormat, css: 'right'},
    {id: 'deposito', header: ['Depósito', {content: 'numberFilter'}],
        width: 125, format: webix.i18n.priceFormat, css: 'right'},
    {id: 'saldo', header: ['Saldo'],
        width: 125, format: webix.i18n.priceFormat, css: 'right'},
]


var grid_cuentabanco = {
    view: 'datatable',
    id: 'grid_cuentabanco',
    select: 'row',
    adjust: true,
    footer: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_cuentabanco_cols,
}


var grid_cfdi_por_pagar_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right'},
    {id: 'id', header: 'ID', hidden: true},
    {id: 'serie', header: 'Serie', adjust: 'data'},
    {id: 'folio', header: 'Folio', adjust: 'data', css: 'right'},
    {id: 'uuid', header: 'UUID', width: 250, hidden: true},
    {id: 'fecha', header: 'Fecha y Hora', width: 150, sort: 'date'},
    {id: 'tipo_comprobante', header: 'Tipo', adjust: 'data'},
    {id: 'estatus', header: 'Estatus', adjust: 'header'},
    {id: 'cliente', header: ['Razón Social', {content: 'selectFilter'}],
        fillspace:true, sort: 'string'},
    {id: 'total', header: ['Total'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right'},
    {id: 'saldo', header: ['Saldo'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right'},
]


var grid_cfdi_este_deposito_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right'},
    {id: 'id', header: 'ID', hidden: true},
    {id: 'serie', header: 'Serie', adjust: 'data'},
    {id: 'folio', header: 'Folio', adjust: 'data', css: 'right'},
    {id: 'uuid', header: 'UUID', width: 250, hidden: true},
    {id: 'fecha', header: 'Fecha y Hora', width: 150, sort: 'date'},
    {id: 'tipo_comprobante', header: 'Tipo', adjust: 'data'},
    {id: 'estatus', header: 'Estatus', adjust: 'header'},
    {id: 'cliente', header: ['Razón Social'], fillspace: true},
    {id: 'total', header: ['Total'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right'},
    {id: 'saldo', header: ['Saldo'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right'},
    {id: 'importe', header: ['Este pago'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right', editor: 'text'},
]


var grid_cfdi_por_pagar = {
    view: 'datatable',
    id: 'grid_cfdi_por_pagar',
    select: 'row',
    multiselect: true,
    autoConfig: false,
    adjust: true,
    height: 250,
    resizeColumn: true,
    headermenu: true,
    drag: true,
    columns: grid_cfdi_por_pagar_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var grid_cfdi_este_deposito = {
    view: 'datatable',
    id: 'grid_cfdi_este_deposito',
    select: 'row',
    autoConfig: false,
    adjust: true,
    height: 200,
    resizeColumn: true,
    headermenu: true,
    drag: true,
    editable: true,
    columns: grid_cfdi_este_deposito_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var toolbar_banco_retiro = [
    {view: 'label', label: 'Agregar retiro de banco'},
    {},
    {view: 'button', id: 'cmd_guardar_retiro', label: 'Guardar Retiro',
        type: 'iconButton', autowidth: true, icon: 'minus'},
    {view: 'icon', click: '$$("multi_bancos").setValue("banco_home")',
        icon: 'times-circle'}
]


var toolbar_banco_deposito = [
    {view: 'label', label: 'Agregar depósito de banco'},
    {},
    {view: 'button', id: 'cmd_guardar_deposito', label: 'Guardar Depósito',
        type: 'iconButton', autowidth: true, icon: 'plus'},
    {view: 'icon', click: '$$("multi_bancos").setValue("banco_home")',
        icon: 'times-circle'}
]


var controls_banco_retiro = [
    {view: 'toolbar', elements: toolbar_banco_retiro},
    {cols: [
        {view: 'datepicker', id: 'date_retiro', name: 'retiro_fecha',
            label: 'Fecha', format: '%d-%M-%Y', labelAlign: 'right',
            required: true, invalidMessage: 'Selecciona una fecha',
            labelWidth: 125},
        {view: 'search', id: 'time_retiro', name: 'retiro_hora',
            label: 'Hora', icon: 'clock-o', labelAlign: 'right',
            pattern:{mask: '##:##:##', allow:/[0-9]/g}, required: true,
            invalidMessage: 'Captura una hora'},
        {view: 'text', id: 'retiro_referencia', name: 'retiro_referencia',
            label: 'Referencia', labelAlign: 'right'},
    ]},
    {cols: [
        {view: 'richselect', id: 'lst_retiro_forma_pago',
            name: 'retiro_forma_pago', label: 'Forma de Pago', required: true,
            options: [], labelWidth: 125, labelAlign: 'right'},
        {view: 'currency', type: 'text', id: 'txt_retiro_importe',
            name: 'retiro_importe', label: 'Importe', labelAlign: 'right',
            required: true, invalidMessage: 'Captura un valor númerico',
            inputAlign: 'right', value: ''}
    ]},
    {cols: [
        {view: 'text', id: 'retiro_descripcion', name: 'retiro_descripcion',
            label: 'Descripción', labelAlign: 'right', required: true,
            labelWidth: 125},
    ]},
]


var controls_banco_deposito = [
    {view: 'toolbar', elements: toolbar_banco_deposito},
    {cols: [
        {view: 'datepicker', id: 'date_deposito', name: 'deposito_fecha',
            label: 'Fecha', format: '%d-%M-%Y', labelAlign: 'right',
            required: true, invalidMessage: 'Selecciona una fecha',
            labelWidth: 125},
        {view: 'search', id: 'time_deposito', name: 'deposito_hora',
            label: 'Hora', icon: 'clock-o', labelAlign: 'right',
            pattern:{mask: '##:##:##', allow:/[0-9]/g}, required: true,
            invalidMessage: 'Captura una hora'},
        {view: 'text', id: 'deposito_referencia', name: 'deposito_referencia',
            label: 'Referencia', labelAlign: 'right'},
    ]},
    {cols: [
        {view: 'richselect', id: 'lst_deposito_forma_pago',
            name: 'deposito_forma_pago', label: 'Forma de Pago', required: true,
            options: [], labelWidth: 125, labelAlign: 'right'},
        {view: 'currency', type: 'text', id: 'txt_deposito_importe',
            name: 'deposito_importe', label: 'Importe', labelAlign: 'right',
            required: true, invalidMessage: 'Captura un valor númerico',
            inputAlign: 'right', value: ''}
    ]},
    {cols: [
        {view: 'textarea', id: 'deposito_descripcion', label: 'Descripción',
            name: 'deposito_descripcion', labelAlign: 'right', required: true,
            labelWidth: 125, height: 70},
    ]},
    {cols: [
        {view: 'label', label: '<b>Facturas por pagar: </b>'},
        {view: 'button', id: 'cmd_invoice_payed', label: 'Pagada directamente',
            type: 'iconButton', autowidth: true, icon: 'check-circle'},
    ]},
    grid_cfdi_por_pagar,
    {view: 'label', label: '<b>Facturas a pagar en este depósito: </b>'},
    grid_cfdi_este_deposito,
]


var form_banco_retiro = {
    type: 'space',
    responsive: true,
    cols: [{
        view: 'form',
        id: 'form_banco_retiro',
        complexData: true,
        scroll: true,
        elements: controls_banco_retiro,
    }],
}


var form_banco_deposito = {
    type: 'space',
    responsive: true,
    cols: [{
        view: 'form',
        id: 'form_banco_deposito',
        complexData: true,
        scroll: true,
        elements: controls_banco_deposito,
    }],
}


var multi_bancos = {
    id: 'multi_bancos',
    animate: true,
    cells:[
        {id: 'banco_home', rows:[
            {view: 'toolbar', elements: toolbar_banco},
            {view: 'toolbar', elements: toolbar_filtro_cuenta},
            {view: 'toolbar', elements: toolbar_movimientos_banco},
            grid_cuentabanco,
        ]},
        {id: 'banco_retiro', rows: [form_banco_retiro]},
        {id: 'banco_deposito', rows: [form_banco_deposito]}
    ],
}


var title_partners = 'Administración de Bancos'
var app_bancos = {
    id: 'app_bancos',
    rows:[
        {view: 'template', id: 'th_bancos', type: 'header',
            template: title_partners},
        multi_bancos
    ]
}


