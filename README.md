# Empresa Libre

## Mini ERP para la legislación mexicana


**En cada relación comercial, hay una relación humana**


Este proyecto está en continuo desarrollo, contratar un esquema de soporte,
nos ayuda a continuar su desarrollo. Ponte en contacto con nosotros para
contratar: administracion ARROBA empresalibre.net


### Requerimientos:

* Servidor web, recomendado Nginx
* uwsgi
* python3
* xsltproc
* openssl
* xmlsec

Debería de funcionar con cualquier combinación servidor-wsgi que soporte
aplicaciones Python.

El sistema tiene soporte para tres bases de datos: SQLite, MySQL y PostgreSQL
(recomendado), debes de instalar el servidor de la base de datos y sus drivers
respectivos, excepto SQLite que es nativo en Python.



